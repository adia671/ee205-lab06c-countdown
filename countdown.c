///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example: 
//   The reference time Tue Jan 21 04:26:07 PM HST 2014 makes counts how long
//   its been since that specific date. (i.e 8 years and a certain amount of 
//   days, hours and minutes. 
// 
//
// @author Adia Cruz <adiacruz@hawaii.edu>
// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>  
#include <time.h>       
#include <unistd.h>

int main(void){

  time_t timer;
  struct tm ref = {0};
  double seconds;
  int years, days, hours, minutes, sec;
  
  ref.tm_hour = 16;
  ref.tm_min = 26;
  ref.tm_sec = 7;
  ref.tm_year = 2014 - 1900;
  ref.tm_mon = 1;
  ref.tm_mday = 21;

  time(&timer);  

  seconds = timer - mktime(&ref);

  printf("Reference time: Tue Jan 21 04:26:07 PM HST 2014\n");
  

  while( seconds ){
	  sleep(1);
	  timer = time(NULL);
	  
	  years = seconds / (31536000); 
	  days = (seconds - years*31536000)/86400;
	  hours = (seconds - years*31536000 - days*86400)/3600;
	  minutes = (seconds -  years*31536000 - days*86400 - hours*3600) / 60;
	  sec = (seconds - years*31536000 - days*86400 - hours*3600 - minutes*60);

	  ++seconds;
	  printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", years, days, hours, minutes, sec);
  } 
  return (0);






	  
}


